import os

# The testing framework needs to know the location of a octez-client
# and the set of example/test contracts distributed in the tezos
# source distribution. This can be set by setting the environment
# variable TEZOS_HOME to a Tezos source distribution containing said
# binary and test contracts. See variables below if the two paths
# needs to be set separately.
TEZOS_HOME = os.environ.get("TEZOS_HOME")
# This must be kept in sync with ../../michelson_reference/Makefile
TEZOS_PROTOCOL_HASH = "ProtoALphaAL"

# OCTEZ_CLIENT should point to a octez-client binary. It defaults to
# TEZOS_home/octez-client, but can be customized through the
# OCTEZ_CLIENT environment variable.
OCTEZ_CLIENT = (os.environ.get("OCTEZ_CLIENT") or
                TEZOS_HOME and f'{TEZOS_HOME}/octez-client')

# TEZOS_CONTRACTS should point to the set of example/test contracts
# distributed in the tezos source distribution. It defaults to
# TEZOS_HOME/michelson_test_scripts, but can be
# customized through the TEZOS_CONTRACTS environment variable.
TEZOS_CONTRACTS = \
  (os.environ.get("TEZOS_CONTRACTS") or
   TEZOS_HOME and
   f'{TEZOS_HOME}/michelson_test_scripts')
