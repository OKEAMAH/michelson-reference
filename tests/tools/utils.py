""" Utility functions for test assertions."""
import subprocess
import re
import contextlib

from typing import List


@contextlib.contextmanager
def assert_run_failure(pattern: str,
                       mode: str = 'stderr'):
    """Context manager that checks enclosed code fails with expected error.

    This context manager checks the contextualized code (in the [with]
    statement) raises [subprocess.CalledProcessError] exception, and
    that the called process stdout or stderr matches [pattern].

    It fails with an [assert False] otherwise.

    Args:
        pattern (Pattern): the pattern that the process output should match
        mode (str): if [mode = stderr], try to match process stderr, otherwise
                    stdout

    Note: the contextualized code must fork a subprocess using
          [subprocess.run] and make sure [capture_output=True].
    """
    # TODO this is similar to the pytest context manager [pytest.raises]
    #      that we already use in some tests. See if we can only use one
    #      construct.

    try:
        yield None
        assert False, "Code ran without throwing exception"
    except subprocess.CalledProcessError as exc:
        stdout_output = exc.stdout
        stderr_output = exc.stderr
        data = []  # type: List[str]
        if mode == 'stderr':
            data = stderr_output.split('\n')
        else:
            data = stdout_output.split('\n')
        for line in data:
            if re.search(pattern, line):
                return
        data_pretty = "\n".join(data)
        assert False, f"Could not find '{pattern}' in {data_pretty}"
    except Exception as exc:  # pylint: disable=broad-except
        assert_msg = f'Expected CalledProcessError but got {type(exc)}'
        assert False, assert_msg
