import argparse

from language_def import LanguageDefinition
import subprocess

def main():
    parser = argparse.ArgumentParser(
        description='Write combined Michelson semantics and meta data.'
    )

    parser.add_argument(
        'output',
        type=argparse.FileType('w+'),
        help='path to output file, i.e. michelson.merged.json')

    args = parser.parse_args()

    lang_def = LanguageDefinition(example_path=None)

    lang_def.write_merged(args.output)

    print(f"Wrote {args.output.name}")


if __name__ == '__main__':
    main()
    
